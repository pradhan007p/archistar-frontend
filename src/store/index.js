import Vue from 'vue'
import Vuex from 'vuex'
import Properties from './modules/properties'
import Filter from './modules/filters'
import Cordinates from './modules/cordinates'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    Properties,
    Filter,
    Cordinates
  }
})
