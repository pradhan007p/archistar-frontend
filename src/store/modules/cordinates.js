import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

const GET_CORDINATES = 'GET_CORDINATES'
const CATCH_ERROR = 'CATCH_ERROR'

export default {
  state: {
    cordinates: [],
    mapErrors: []
  },

  getters: {
    cordinates: state => state.cordinates,
    mapErrors: state => state.mapErrors
  },

  mutations: {
    [GET_CORDINATES] (state, payload) {
      const cordinates = payload.features.map(function (category) {
        if (payload.filter.suburb !== '' || payload.filter.category !== '') {
          if (payload.filter.category !== '' && payload.filter.suburb !== '') {
            // Get the property data if both the filters values are set
            if (payload.filter.category === category.properties.project.Category && payload.filter.suburb === category.properties.project.Suburb) {
              return (category.properties)
            }
          } else if (payload.filter.category !== '') {
            // Get the property data if category filters value is set
            if (payload.filter.category === category.properties.project.Category) {
              return (category.properties)
            }
          } else if (payload.filter.suburb !== '') {
            // Get the property data if suburb filters value is set
            if (payload.filter.suburb === category.properties.project.Suburb) {
              return (category.properties)
            }
          }
        } else {
          // Get all the property data if no filters value is set
          return (category.properties)
        }
      })

      const filtered = cordinates.filter(function (el) {
        return el != null
      })

      state.cordinates = filtered
    },

    [CATCH_ERROR] (state, payload) {
      state.mapErrors = payload
    }
  },

  actions: {
    getCordinates (context, filter) {
      return axios.get('/data/testBlob.json').then(response => {
        response.data.filter = filter // Assign selected filter
        context.commit(GET_CORDINATES, response.data)
      }, error => {
        context.commit(CATCH_ERROR, error.data)
      })
    }
  }
}
