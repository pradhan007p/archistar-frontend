import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

const GET_PROPERTY_CATEGORY = 'GET_PROPERTY_CATEGORY'
const CATCH_ERROR = 'CATCH_ERROR'

export default {
  state: {
    categories: [],
    suburbs: [],
    errors: []
  },

  getters: {
    categories: state => state.categories,
    suburbs: state => state.suburbs,
    errors: state => state.errors
  },

  mutations: {
    [GET_PROPERTY_CATEGORY] (state, payload) {
      const categoriesList = []
      const suburbList = []

      payload.forEach(obj => {
        // Create Category Lists
        const category = obj.properties.project.Category
        if (categoriesList.indexOf(category) < 0) {
          categoriesList.push(category)
        }

        // Create Suburb Lists
        const suburb = obj.properties.project.Suburb
        if (suburbList.indexOf(suburb) < 0) {
          suburbList.push(suburb)
        }
      })

      state.categories = categoriesList
      state.suburbs = suburbList
    },

    [CATCH_ERROR] (state, payload) {
      state.errors = payload
    }
  },

  actions: {
    getCategories (context) {
      return axios.get('/data/testBlob.json').then(response => {
        context.commit(GET_PROPERTY_CATEGORY, response.data.features)
      }, error => {
        context.commit(CATCH_ERROR, error.data)
      })
    }
  }
}
