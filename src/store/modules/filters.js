import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const SET_FILTERS = 'SET_FILTERS'

export default {
  state: {
    filters: {}
  },

  getters: {
    filters: state => state.filters
  },

  mutations: {
    [SET_FILTERS] (state, payload) {
      state.filters = payload
    }
  },

  actions: {
    setFilters (context, data) {
      context.commit(SET_FILTERS, data)
    }
  }
}
